@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    <video id="my-video" class="video-js" controls preload="auto" width="640" height="264"
					  poster="video/big_buck_bunny.jpg" data-setup="{}">
						<source src="video/big_buck_bunny.mp4" type='video/mp4'>
						<source src="video/big_buck_bunny.webm" type='video/webm'>
						<p class="vjs-no-js">
						  To view this video please enable JavaScript, and consider upgrading to a web browser that
						  <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
						</p>
					</video>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
